（插圖aid_9234）

手離開魔法陣，深深地嘆了口氣。
取了一根亂扔在桌子上的香煙，點著火。

真是意想不到的即興台詞。我愛著７號，這句話完全是片面斷定。
雖然對７號很抱歉，但我沒有傷感到那種程度。無論怎麼說，這是我們的生意。如果真的愛著自動人偶的話，怎麼會讓她們干像性愛機器人一樣的事呢。

儘管如此，這次的事件完全是我不好。應該謝個罪吧。

「抱歉啊，７號。就因為主人無能，讓你陷入了不幸啊」

結果，即使７號多少也有做過頭的地方，她也幾乎沒有加害人。
明明被人類虐待了。明明被像奴隷一樣對待了。
儘管優先度（Ｐ）制御（Ｃ）魔法（Ｃ）沒有發揮作用。儘管能自由地行動。

像７號一樣的人一定會被用天使這個名字稱呼吧。
向天花板吐出香煙的煙。把煙當作靈魂，祈求了沒有靈魂的她的冥福。

那麼，這件事應該告訴市子醬嗎，應該不告訴嗎。
不告訴比較好啊。因為市子醬絶對會哭的。
一邊想著該怎麼辦一邊回頭看後面，大吃了一驚。
在房間的角落佇立的５號正在流淚。

「怎，怎麼了，５號？」
「欸？啊啦，討厭。為什麼，我」

喂喂，這傢伙也是嗎。明明人類的我一滴眼淚沒流正忙於事後處理，自動人偶卻誰都隨意地撲簌落淚。

簡直就像全員都在向我發出責難之聲一樣不是嗎。
無論怎樣哭，無論怎樣責難都沒關係。必須做的事就是必須做，不能做的事就是不能做。

我再次把手放在魔法陣上，選擇被保管在假想存儲器上的７號的記憶域數據。
消去這個數據的話，一切就結束了。

消滅證據也完成了。我製作了擁有自我的不能制御的自動人偶的不光彩這樣子也能勾銷了。
雖然是容量大得驚人的數據，但只是消去的話不會那麼花時間。
雖然被上了非常堅固的鎖，但我掌握了密碼，從７號那問出了想問的事的現在，終於能解除鎖、消除數據了。

正想著來吧開始最後的作業吧的時候，我注意到了。不知什麼時候，鎖被解除了。

７號的世界的數據完全無抵抗、無防備地被暴露著。
心想這咋回事，不過馬上就明白了這是那孩子幹的。
覺得７號一邊在說『請，把我，徹底消除』，一邊在笑。

──製作了我，愛著我，謝謝，主人。

啊啊，吵死了。

明明抵抗一下的話，我也不會這麼感到罪惡感了。
明明對製作了你的主人的我，哪怕說一句怨言都好。
我不是為了讓你在最後說那種台詞才製作你的。

不需要感謝我。
因為你就是為了被愛才出生的。

──所以，不要再笑得那麼悲傷了，７號。

「５號」

響應我的呼喚，５號小跑著來了。

「有什麼事嗎」
「假如，我說『接下來我要做犯罪行為，當作沒看見』的話，你會怎麼辦？」

５號顯示了啞然的表情，之後打心眼裡高興地露出了笑臉。

「我會當作沒看見」
「喂喂，別那麼簡單地聽命令啊。自動人偶由於優先度（Ｐ）制御（Ｃ）魔法（Ｃ）不能袒護犯罪行為」
「朝霧人偶工房的自動人偶的最高Priority是，『絶對服從主人』」

不愧是５號。大正解。

「７號，就拜託你了」
「哈。是什麼事呢」

裝糊塗沉默了。

歸根結底，這不是犯罪行為。
只是忘記消除來源不明的數據，先放置了而已。

但是，反正要回來的話，最好是早一點。
結果，決定為我的孩子準備道路後，我立刻開始了作業。

對自己的笨蛋家長的樣子無語了，但這也沒辦法吧。
因為她們，存在於這個世上的自動人偶，沒有一個例外，都是為了被愛才出生的。

＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊