# novel

- title: 万年Ｄランクの中年冒険者、酔った勢いで伝説の剣を引っこ抜く
- title_zh: 萬年D等級的中年冒險者、藉著酒勢拔出了傳說之劍
- author: 九頭七尾
- illust:
- source: http://ncode.syosetu.com/n0608dy/
- cover: https://images-na.ssl-images-amazon.com/images/I/7135VnVMHWL.jpg
- publisher: syosetu
- date: 2019-05-25T19:00:00+08:00
- status: 連載
- novel_status: 0x0300

## illusts

- 温木アツシ
- へいろー
- 

## publishers

- 

## series

- name: 万年Ｄランクの中年冒険者、酔った勢いで伝説の剣を引っこ抜く

## preface


```
【コミック1巻は3月13日発売！】
万年Ｄランクのしがない中年冒険者ルーカスは、自分よりずっと若いパーティメンバーたちに見下され、扱き使われる毎日を送っていた。
だが「飲まずにやってられっか！」と飲んだくれたある夜のこと、酔った勢いで今まで誰も抜くことができなかった伝説の剣を引き抜いてしまう。
愛と勝利の女神が造ったという神剣の力で、おっさんは（望まぬ）ハーレムを作りながら（望まぬ）英雄の道を駆け上がっていく…！
```

## tags

- node-novel
- R15
- syosetu
- やがて主人公最強
- エロ剣
- ハイファンタジー
- ハイファンタジー〔ファンタジー〕
- ハーレム展開
- ファンタジー
- 俺ＴＵＥＥＥ
- 残酷な描写あり
- 神剣
- 英雄
- 魔剣
- esjzone
- 

# contribute

- 毛茸茸的薯
- 問如何邂逅病嬌
- 章魚子
- 來異世界之旅
- 

# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 1
- startIndex: 0

## syosetu

- txtdownload_id:
- series_id:
- novel_id: n0608dy

## esjzone

- novel_id: 1546060392

## textlayout

- allow_lf2: false

# link

- [narou.nar.jp](https://narou.nar.jp/search.php?text=n0608dy&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [小説情報](https://ncode.syosetu.com/novelview/infotop/ncode/n0608dy/)
- http://www.dm5.com/manhua-wannian-d-jidezhongnianmaoxianzhe-jiezhejiushibachulechuanshuozhijian/
- https://www.ganganonline.com/contents/mannen_d/
- [万年d等级的中年冒险者吧](https://tieba.baidu.com/f?kw=%E4%B8%87%E5%B9%B4d%E7%AD%89%E7%BA%A7%E7%9A%84%E4%B8%AD%E5%B9%B4%E5%86%92%E9%99%A9%E8%80%85&ie=utf-8 "万年d等级的中年冒险者")
- https://masiro.moe/forum.php?mod=forumdisplay&fid=96
- 
