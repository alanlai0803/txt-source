本來，語言是沒有力量的。

不能移動事物，絲毫無法改變世界。畢竟語言不過是聲音的羅列。但是不知道為什麼？確實存在著被語言那種東西奪走心神的人。

芙拉朵教過我那個嗎？而且，事到如今還不允許逃跑。真是，在那種地方和以前完全一樣的傢伙。

那就做給你看。眼前揮舞著槍的都市兵們，只是一些被羅佐的話語迷惑住大腦的人。正因為如此，再一次影響其思想就行了，他們的思想只是被注入的熱情，被惡意灌輸的狂熱所壓倒的東西。

「你經常提阿爾蒂斯是萬能的救濟神呢，那真是讓人落淚的信仰心。太棒了，真是令我感動。但是，這些虛言只在大聖教教堂中通用哦，羅佐。」

簡直就像是在說明顯的事實一般。路易斯縮緊肩膀，用鼻子笑著，準備在對方不經意間就打敗其語言上建立的第一個據點。
要高效地欺騙人，那是最好的方法。往前進了一步。臉頰上浮現著微笑，我明白了我的語言有效了。

可以看到菲洛斯都市兵的眼睛裡的神情，與其說這些神情是憤怒和侮蔑，不如說是充滿了驚愕。他們至今為止都在菲洛絲這個小城市中生活，周圍大概沒有哪個人會堂堂正正地貶低阿爾蒂斯吧。正因為如此，這一點才應該加以利用。

「不敬畏神的話語，只是為你而存在的吧，惡德的路易斯。雖然不能要求你慈悲，但至少你要知道羞恥。」

對，羅佐的行為堂堂正正，正適合侍奉神靈。那麼，我應該要侍奉著誰而行動呢？我不信奉紋章教的神，那麼應該是信奉聖女瑪蒂婭吧，或者是精靈公主艾爾蒂絲？這麼說來，不管怎樣，我侍奉誰的這部分還是很曖昧的。

但是，如果要我從神或惡魔這兩個傢伙中選擇一個來侍奉的話，毫無疑問惡魔要強一點吧。我怎麼也想像不到自己死後神來迎接我的景象。
對在一旁不安地晃動眼睛的拉爾格・安，我很輕地點了下頭，然後繼續說。

「我想把那句話原封不動地還給你，羅佐什麼的傢伙。知恥為妙，如果阿爾蒂斯真是萬能的話，為什麼我現在還能活在這裡呢？如果違抗了萬能之神，難道不是早就應該被土地埋葬了嗎？」

像是在嘲笑羅佐的話一樣，用像教會他明明白白的事實一樣的口吻說。

欺騙人的時候什麼因素最重要？

那就是自己也相信那個謊言。嘴巴，動作，表情，一切都委託給了那個謊言。隨著胡亂的思想而歪曲自己就好。正因為如此，人才會被騙。因此不容易欺騙自己的人，本來就不會是那種被人欺騙的人，如果這次是對上這種人，就應該馬上轉身逃走了。

但是，現在眼前是曾經被欺騙過一次的人們。感覺我能看見羅佐的嘴唇歪斜了。

「神賜予人類考驗。在苦難中，把信仰托付給神的人會得到拯救。你可說是神賜予我們的考驗──」

我立刻把他說出來的話吞噬掉。考驗什麼的，不就是他們最喜歡的話語嗎？

「──你真的相信這種事嗎，羅佐。根據大聖教的教誨，原本背德者應該馬上就會受到天罰吧。怎麼樣，雖然我原本就是大聖教徒，但直到成為紋章教徒的現在我還活著。」

因為我小時候甚至去過大聖教教堂，一邊這樣在內心補充道。周圍人們的目光都動搖了，被「背德者」一詞充斥著。
來，勾引一下吧。與其討論遠方的神，不如用更加親近的、大惡的語言抓住你們的心臟。

「這不是考驗，羅佐。只是，就連阿爾蒂斯也沒能殺死我。明明我向大聖教吐唾沫，踐踏其教義。但是即使使用大聖教的軍隊，也不能讓我流血。」

事實上在戰場上盡情地流血了，當然適度的誇張倒也挺好。因為在給人講故事的時候，稍微虛榮一下是很重要的。謙虛和誠實是優點，但有時候也是使人價值暴跌的缺點。
張著嘴，視線從羅佐轉向周圍的都市兵，一邊抑制著快要扭曲的表情，一邊只是笑著說。

「那麼，你們要殺死哪裡的誰呢？連大聖教的軍隊，連神都殺不了我。為什麼，你們感覺能殺了我？」

張開雙臂，宛如迎接朝向我的槍一般。你們絶對殺不死我，這麼告訴菲洛絲的都市兵，還有紋章教的士兵們。
再前進一步。為了讓大家看到，自己沒有任何可以擔心的事情，沒有任何猶豫和動搖。

實際上，心臟像被大雨淋了一樣地搖擺著，腳後跟上有寒氣升起，背部淌著冷汗。有這種反應是理所當然的，我現在的表演簡直就像是在鋼絲上行走一般。

這時都市兵中哪怕有一個人發出吶喊揮舞著槍向我刺過來，那我的表演就瞬間泡湯化為喜劇了。如果真發生這種情況，恐怕周圍的人都會像被熱浪沖昏頭一般撲向我吧。

是的，問題就是那種熱情。那些菲洛絲兵現在拿槍與紋章教對抗的原因，是因為參與的每個人都沉浸在羅佐那傢伙發起的狂熱中。
那麼我把這個狂熱從他們腦海中拿掉，換成別的東西就好了。比如，恐怖、恐懼之類的東西。

「⋯⋯真能開玩笑，大惡之徒。神會以你的終結告訴我們吧。你和菲洛絲・托雷特這樣的背德者，會屍骨無存的！」

在那粗暴的聲音中，稍稍潛藏著一些事實的影子。
原來如此，我以為菲洛絲・托雷特也屈服於民會的壓力，對抗紋章教了。看來情況是相反的。和我們結成同盟後，她被污蔑為背德者，被趕下台了嗎？

真是不順心。在這個世界上，正義的東西被玷污，虛言反而受到大家歡迎。因為這樣扭曲的事情堂而皇之地公然上演，因此果然還是不能相信神的。

扭曲著嘴唇，看著都市士兵們。真正想救你們的，除了菲洛絲・托雷特以外沒有其他人了吧。不管怎麼說，應該說這些傢伙是被厄運纏身了吧，被虛言欺騙真是可怜啊。
一邊捕捉著耳朵裡聽到的一個聲音，一邊說著抵消羅佐粗暴語言的話語。

「──你果然這麼說了啊，什麼羅佐之類的傢伙。那麼阿爾蒂斯啊，現在就在這裡現身殺死我吧。用萬能的力量什麼的，貫穿我的心臟吧！」

向上看著，向天說話。無論怎樣向空中吶喊，天氣還是晴朗的。一點也沒有要降下天罰的跡象。看來今天晚上可以心情很好地喝酒了。

耳朵裡開始回響起馬車奔跑的聲音。這絶不是菲洛絲都市士兵所驅使的馬車或軍馬的蹄聲。從這些聲音交織在一起的節奏來看，以我的經驗，過來的大多是僱傭兵或冒険者那樣的傢伙。聽說過段時間會有一支僱傭兵部隊過來與大軍合流，但現在他們出場的話不是也做得很好嘛。

那麼稍微打扮一下吧。因為要和久違的好友的相遇。路易斯縮緊肩膀，挺起胸膛。這樣一來，即使完全弄錯了來人的身份，也不會留下什麼恥辱了。

「那麼，是誰要殺了我呢？嗯？」

為了把周圍的人全部卷進這種情況，這樣說了。馬蹄聲似乎很快就會來到身後。